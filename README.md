# uQlick

*Note: this readme was adapted and updated from the original Qlicker readme for uQlick.*

[uQlick](https://uqlick.com) is a web application that allows professors to quiz students on material during a lecture or course session using laptops and smartphones. It is based off of the open-source [Qlicker](https://github.com/qlicker/qlicker) software, but with several improvements:

* **More flexibility with questions**
    * Multiple choice and multiple select questions can be set to shuffle for students.
    * Numerical entry questions can have be set to use decimal, hexadecimal (base 16), binary (base 2), or octal (base 8) number systems.

* **Help documentation built-in**
* **User interface upgraded to Bootstrap 4**

## Using uQlick

The user documentation for the publicly hosted version of uQlick is available at https://uqlick.com/help. When running your own instance of uQlick, the user documentation will be at `[address]/help` respectively.

## Running uQlick locally

1. Install Meteor
```
curl https://install.meteor.com/ | sh 
```

2. Clone the uQlick repository to your machine and install the necessary node packages.
```
meteor npm install
```

Note, if you use `npm install` instead of `meteor npm install`, you will need to manually delete the `node_modules` directory, and then run `meteor npm install`.

3. Run the program.
```
meteor
```

To run tests locally
`npm run test:unit-watch` or `npm run test:app-watch`

## Deploying uQlick to a remote server
We use [Meteor Up](http://meteor-up.com) to deploy uQlick to our public server. Follow the documentation at http://meteor-up.com/getting-started.html for getting uQlick running.


