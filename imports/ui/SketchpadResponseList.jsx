/* global MathJax */
// QLICKER
// Author: Enoch T <me@enocht.am>, modified by Malcolm Anderson <malcolminyo@gmail.com>
//
// ShortAnswerList.jsx

import React, { Component, PropTypes } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import { WysiwygHelper } from '../wysiwyg-helpers'
import { Responses } from '../api/responses'
import { SketchPadInput } from './SketchPadInput'
/**
 * React component (meteor reactive) that displays a list of sketchpad reponses for a question
 * @prop {Question} question - question object
 */
class _SketchpadResponseList extends Component {

  renderAnswer (r) {
    const user = Meteor.users.findOne(r.studentUserId)
    const name = user ? user.getName() : 'Unknown'
    const answer = r.answer
    console.log(answer)
    return (
      <div>
        <SketchPadInput height={200} width={200} readOnly={true} strokes={answer} />
        <br />
        {name}
      </div>
    )
  }

  render () {
    if (this.props.loading) return <div>Loading</div>
    return (<div className='ql-short-answer-list'>
      <h3>Responses</h3>
      {
        this.props.responses.map(r => <div key={r._id} className='ql-short-answer-item'>{this.renderAnswer(r)}</div>)
      }
    </div>)
  } //  end render

}

export const SketchpadResponseList = createContainer((props) => {
  const handle = Meteor.subscribe('responses.forQuestion', props.question._id)
  const question = props.question
  const attemptNumber = question.sessionOptions ? question.sessionOptions.attempts.length : 0
  // Get the responses for that attempt:
  const responses = Responses.find({ questionId: question._id, attempt: attemptNumber }, { sort: { createdAt: -1 } }).fetch()
  // const responses = Responses.find({ questionId: props.question._id }, { sort: { createdAt: -1 } }).fetch()

  return {
    responses: responses,
    loading: !handle.ready()
  }
}, _SketchpadResponseList)

SketchpadResponseList.propTypes = {
  question: PropTypes.object.isRequired
}
