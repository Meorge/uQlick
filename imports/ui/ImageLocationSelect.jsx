import React, { Component } from 'react'

export class ImageLocationSelect extends Component {

    constructor (props) {
        super(props)
        this.state = {clickedX: 0, clickedY: 0}
        this.props = props

        this.onClick = this.onClick.bind(this)

        this.redraw = this.redraw.bind(this)

        this.selectionHeight = 40
        this.selectionWidth = 40

    }

    componentDidMount() {
        this.redraw()
    }
    componentDidUpdate() {this.redraw()}

    redraw() {
        const canvas = this.refs.canvas

        if (canvas != null) {
            const ctx = canvas.getContext("2d")
            const img = this.refs.image

            ctx.clearRect(0,0,canvas.width,canvas.height);
            ctx.drawImage(img, x=0, y=0, width=canvas.width, height=canvas.height)

            img.onload = () => {
                console.log("draw image")
                ctx.drawImage(img, x=0, y=0, width=canvas.width, height=canvas.height)
            }

            let rect = canvas.getBoundingClientRect();
            let x = this.state.clickedX - rect.left; 
            let y = this.state.clickedY - rect.top; 

            console.log("do the redrawing")
            // ctx.fillStyle = '#FF00FF'
            // ctx.fillRect(0,0,500,500)

            ctx.fillStyle = '#00000060'
            ctx.strokeStyle = '#000000'
            ctx.lineWidth = 3
            ctx.fillRect(x - this.selectionWidth / 2, y - this.selectionHeight / 2, this.selectionWidth, this.selectionHeight)
            ctx.strokeRect(x - this.selectionWidth / 2, y - this.selectionHeight / 2, this.selectionWidth, this.selectionHeight)
        }
    }

    onClick(e) {
        console.log(e.clientX, e.clientY)
        this.setState({clickedX: e.clientX, clickedY: e.clientY})
    }
  
    render() {
        let output = (
            <div className=''>
                <canvas ref="canvas" height={500} width={500} onClick={this.onClick} />
                <img ref="image" src='/images/poster.jpg' className='hidden' height={300} width={300} />
            </div>
        )

        return output
    }
  }