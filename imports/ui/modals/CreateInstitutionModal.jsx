// QLICKER
// Author: Enoch T <me@enocht.am>
//
// CreateCourseModal.jsx

import React from 'react'
import _ from 'underscore'

import { ControlledForm } from '../ControlledForm'
import { Institutions } from '../../api/institutions'

export const FORM_INPUTS = {
  name: ''
}

export const DEFAULT_STATE = Object.assign({}, FORM_INPUTS, {
  name: "Institution",
  confirmedCreate: false
})

/**
 * modal dialog to prompt for course details
 * @augments ControlledForm
 * @prop {Func} done - done callback
 */
export class CreateInstitutionModal extends ControlledForm {

  constructor (props) {
    super(props)
    this.state = _.extend({}, DEFAULT_STATE)
    
  }

  componentDidMount() {
  }

  /**
   * handleSubmit(Event: e)
   * onSubmit handler for course form. Calls courses.insert
   */
  handleSubmit (e) {
    super.handleSubmit(e)

    const newInst = {
        name: this.state.name,
        instructors: [Meteor.userId()],
        createdAt: new Date(),
        requireVerified: false
    }

    Meteor.call('institutions.insert', newInst, (e, data) => {
        if (e) alertify.error(e.reason)
        else alertify.success('Institution created')
        this.done(e)
    })
  }

  /**
   * done(Event: e)
   * Overrided done handler
   */
  done (e) {
    if (this.refs.createinstForm) this.refs.createinstForm.reset()
    this.setState(_.extend({}, DEFAULT_STATE))
    super.done()
  }

  render () {
    return (<div className='ql-modal-container' onClick={this.done}>
      <div className='ql-modal ql-modal-createcourse ql-card' onClick={this.preventPropagation}>
        <div className='ql-modal-header ql-header-bar'>
          <button type="button" className="close float-left" aria-label="Close" onClick={() => {
                this.setState({confirmedCreate: false})
                this.done({})
              }}>
            <span aria-hidden="true">&times;</span>
          </button>
          <h3>Create Institution</h3>
          </div>
        { !this.state.confirmedCreate ?
        (<div className='ql-form-createcourse ql-card-content'>
          <b className='text-center'>Are you sure you want to create a new institution?</b>
          <p>An institution only needs to be created once. If you are a professor, you may only need to be added to an existing institution.</p>
          <button className='btn btn-default btn-success' onClick={() => (this.setState({confirmedCreate: true}))}>Create an Institution</button>
              <button className='btn btn-default btn-outline-success' onClick={() => {
                this.setState({confirmedCreate: false})
                this.done({})
              }}>No thanks</button>

        </div>) : 
        (<form ref='createinstForm' className='ql-form-createcourse ql-card-content' onSubmit={this.handleSubmit}>
          <label>Name:</label>
          <input type='text' className='form-control' data-name='name' onChange={this.setValue} placeholder='Harvard University' /><br />

          <div className='ql-buttongroup'>
            <a className='btn btn-default' onClick={this.done}>Cancel</a>
            <input className='btn btn-default' type='submit' id='submit' />
          </div>
        </form>) }
      </div>
    </div>)
  } //  end render

} // end CreateCourseModal
