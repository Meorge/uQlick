import React, { Component } from 'react'
import Helpers from '../api/helpers'

export class SketchPadInput extends Component {

    constructor (props) {
        super(props)

        // generate a random code to identify this sketchpad
        var myCode = Helpers.RandomString(6)
        this.state = {
            color: "black",
            strokeWidth: 6,
            width: this.props.width,
            height: this.props.height,
            readOnly: this.props.readOnly,
            code: myCode
        }

        this.doBeep = this.doBeep.bind(this)
        this.sketchpadChanged = this.sketchpadChanged.bind(this)

        this.getSketchpadInfo = this.getSketchpadInfo.bind(this)
        this.loadSketchpadInfo = this.loadSketchpadInfo.bind(this)
    }

    componentDidMount() {
        this.redraw();
    }

    componentDidUpdate(prevProps) {
        console.log("componentDidUpdate() - this.props.strokes = ", this.props.strokes)
        if (prevProps.strokes !== this.props.strokes) {
            this.loadSketchpadInfo(this.props.strokes)
        }

        if (prevProps.readOnly !== this.props.readOnly) {
            console.log("readonly updated to", this.props.readOnly);
            this.sketchpad.setReadOnly(this.props.readOnly);
        }
    }
    doBeep() {
        console.log("BEEP!");
    }

    sketchpadChanged() {
        console.log("SKETCHPAD CHANGED, STROKES = ", JSON.stringify(this.getSketchpadInfo()))

        if (this.props.onChange) this.props.onChange(this.getSketchpadInfo())
    }

    redraw() {
        const canvas = this.refs["canvas-" + this.state.code];

        this.sketchpad = new Sketchpad({
            backgroundColor: "white",
            element: "#sketchpad-" + this.state.code,
            width: this.state.width,
            height: this.state.height,
            readOnly: this.props.readOnly,
            onChanged: this.sketchpadChanged
        })

        if (this.props.strokes) {
            console.log("load content", this.props.strokes)
            this.loadSketchpadInfo(this.props.strokes)
        }
        
        this.setStrokeWidth(this.state.strokeWidth)
        this.setColor("black")

    }

    clearCanvas() {
        this.sketchpad.clear()
    }
    setStrokeWidth(width) {
        this.sketchpad.penSize = width
        this.setState({strokeWidth: width})
    }
    setColor(col) {
        this.sketchpad.color = col
        this.setState({color: col})
    }


    printStuff() {
        console.log(JSON.stringify(this.getStrokes()))
    }

    setReadOnly(r) {
        this.sketchpad.readOnly = r
        this.setState({readOnly: r})

    }


    applyStrokes(strokes) {
        if (strokes == null) return
        console.log("applyStrokes() - original strokes:", JSON.stringify(strokes))
        strokes.forEach(function(part, strokeIndex, strokeArray) {
            var singleStroke = strokeArray[strokeIndex]
            var strokeLines = singleStroke.lines
            strokeLines.forEach(function(part, lineSegmentIndex, lineSegmentArray) {
                var lineSegment = lineSegmentArray[lineSegmentIndex]
                lineSegment.start.x *= this.state.width
                lineSegment.start.y *= this.state.height
                lineSegment.end.x *= this.state.width
                lineSegment.end.y *= this.state.height
                lineSegmentArray[lineSegmentIndex] = lineSegment
            }, this);
        }, this)

        console.log("applyStrokes() - adjusted strokes:", JSON.stringify(strokes))

        this.sketchpad.redraw(strokes)
    }

    loadSketchpadInfo(info) {
        // let's correct the proportions for the sketchpad drawings...
        console.log("loadSketchpadInfo() - info = ", info)
        // this.sketchpad.clear(false);
        // this.sketchpad.context.clearRect(0, 0, this.sketchpad.canvas.width, this.sketchpad.canvas.height);
        if (info == null || info == "") {
            console.log("THERES NOTHING TO PUT ON THE SKETCHPAD");
            this.sketchpad.clear(false)
            // this.sketchpad.context.clearRect(0, 0, this.sketchpad.canvas.width, this.sketchpad.canvas.height);
            return;
        }
        var strokes = info.strokes

        console.log("loadSketchpadInfo() - before correction:", JSON.stringify(info), JSON.stringify(strokes))
        strokes.forEach( function(part, strokeIndex, strokeArray) {
            strokeArray[strokeIndex].size = (strokeArray[strokeIndex].size * (this.state.width)) / (info.width);
            strokeArray[strokeIndex].lines.forEach( function(part, lineSegIndex, lineSegArray) {
                lineSegArray[lineSegIndex].start.x = (lineSegArray[lineSegIndex].start.x * this.state.width) / info.width;
                lineSegArray[lineSegIndex].start.y = (lineSegArray[lineSegIndex].start.y * this.state.height) / info.height;
                lineSegArray[lineSegIndex].end.x = (lineSegArray[lineSegIndex].end.x * this.state.width) / info.width;
                lineSegArray[lineSegIndex].end.y = (lineSegArray[lineSegIndex].end.y * this.state.height) / info.height;
            }, this);
        }, this);

        console.log("loadSketchpadInfo() - after correction:", JSON.stringify(strokes))
        this.sketchpad.redraw(strokes)

    }

    getSketchpadInfo() {
        // like getStrokes() but maybe it'll work???
        if (!this.sketchpad) return null
        if (!this.sketchpad.strokes) return null
        var sketchpadInfo = JSON.parse(this.sketchpad.toJSON())
        // sketchpadInfo.undoHistory = []
        console.log("getSketchpadInfo() - sketchpad info is now ", sketchpadInfo)
        return sketchpadInfo
    }

    getStrokes() {
        if (!this.sketchpad) return null
        if (!this.sketchpad.strokes) return null
        var _strokes = JSON.parse(JSON.stringify(this.sketchpad.strokes))

        // console.log("getStrokes() - original strokes:", JSON.stringify(_strokes))
        // console.log("state: ", this.state)
        _strokes.forEach(function(part, strokeIndex, strokeArray) {
            var singleStroke = strokeArray[strokeIndex]
            var strokeLines = singleStroke.lines
            strokeLines.forEach(function(part, lineSegmentIndex, lineSegmentArray) {
                var lineSegment = lineSegmentArray[lineSegmentIndex]
                lineSegment.start.x /= this.state.width
                lineSegment.start.y /= this.state.height
                lineSegment.end.x /= this.state.width
                lineSegment.end.y /= this.state.height
                lineSegmentArray[lineSegmentIndex] = lineSegment
            }, this);
            // console.log("strokeLines = ", strokeLines)

            singleStroke.lines = strokeLines
            strokeArray[strokeIndex] = singleStroke
        }, this)

        console.log("getStrokes() - adjusted strokes:", JSON.stringify(_strokes))

        return _strokes


    }


    /*
    TODO (18 March 2020)
    We want to get it so that strokes are loaded in from run_session.jsx and rendered... right now it seems like it's deleting them or something?


    */
    render() {
        let output = (
            <div className="d-flex">
                <div className="container">
                    <canvas ref={"canvas-" + this.state.code} className="row sketchpad" id={"sketchpad-" + this.state.code}></canvas>
                    {this.props.readOnly ? '' : <div>
                        <div className="btn-toolbar row" role="toolbar">
                            <div className="btn-group mr-2" role="group">
                                <button type="button" className="btn btn-secondary" onClick={() => {this.clearCanvas()}}><i className="material-icons">clear</i></button>
                            </div>

                            {/* <div className="btn-group mr-2" role="group">
                                <button type="button" className="btn btn-secondary" onClick={() => {this.sketchpad.undo()}}><i className="material-icons">undo</i></button>
                                <button type="button" className="btn btn-secondary" onClick={() => {this.sketchpad.redo()}}><i className="material-icons">redo</i></button>
                            </div> */}

                            <div className="btn-group mr-2" role="group">
                                <button type="button" className="btn btn-secondary" onClick={() => {this.setStrokeWidth(3)}}><i className={"material-icons sketchpad_" + this.state.color} id="sketchpad_smallicon">brightness_1</i></button>

                                <button type="button" className="btn btn-secondary" onClick={() => {this.setStrokeWidth(6)}}><i className={"material-icons sketchpad_" + this.state.color} id="sketchpad_mediumicon">brightness_1</i></button>

                                <button type="button" className="btn btn-secondary" onClick={() => {this.setStrokeWidth(10)}}><i className={"material-icons sketchpad_" + this.state.color} id="sketchpad_largeicon">brightness_1</i></button>

                                <button type="button" className="btn btn-secondary" onClick={() => {this.setStrokeWidth(20)}}><i className={"material-icons sketchpad_" + this.state.color} id="sketchpad_xlicon">brightness_1</i></button>

                            </div> 

                            <div className="btn-group mr-2" role="group">
                                <button id="colorDropdown" type="button" className="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i className="material-icons">color_lens</i></button>
                                <div className="dropdown-menu" aria-labelledby="colorDropdown">
                                    <a className="dropdown-item" onClick={() => {this.setColor("black")}}><i className="material-icons sketchpad_black">brightness_1</i> Black</a>
                                    <a className="dropdown-item" onClick={() => {this.setColor("red")}}><i className="material-icons sketchpad_red">brightness_1</i> Red</a>
                                    <a className="dropdown-item" onClick={() => {this.setColor("green")}}><i className="material-icons sketchpad_green">brightness_1</i> Green</a>
                                    <a className="dropdown-item" onClick={() => {this.setColor("blue")}}><i className="material-icons sketchpad_blue">brightness_1</i> Blue</a>
                                    <a className="dropdown-item" onClick={() => {this.setColor("yellow")}}><i className="material-icons sketchpad_yellow">brightness_1</i> Yellow</a>
                                </div>

                            </div> 
                        </div>
                    </div>}
                </div>
            </div>
        )

        return output
    }
  }