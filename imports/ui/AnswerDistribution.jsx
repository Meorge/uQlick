// QLICKER
// Author: Enoch T <me@enocht.am>
//
// AnswerDistribution.jsx

import React, { Component, PropTypes } from 'react'
import { createContainer } from 'meteor/react-meteor-data'
import { _ } from 'underscore'

import { PieChart, Pie, BarChart, Bar, XAxis, YAxis, Legend } from 'recharts'

/**
 * React Component (meteor reactive) for attempt distributions for a question
 * @prop {Question} question - question object
 */
export class _AnswerDistribution extends Component {
  constructor (props) {
    super(props)

    this.state = {
      chartMode: "pie"
    }

    console.log("construction is done")


  }
  getRandomColor () {
    let letters = '0123456789ABCDEF'
    let color = '#'
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)]
    }
    return color
  }

  generatePieChartArray() {
    const latestAttempt = this.props.maxAttempt
    const distrib = this.props.distribution

    const listOfOptions = []
    distrib.map((a) => {
      var newThing = {}
      newThing["name"] = "Option"
      newThing["value"] = a["attempt_" + latestAttempt]
      listOfOptions.push(newThing)
    })

    return listOfOptions
  }

  componentDidMount() {
    this.setState({chartMode: "pie"})
  }

  // render() {
  //   return this.renderChart();
  // }


  render() {
    var out = []
    if (!this.state.chartMode) {
      console.log("chartMode didn't exist so now its pie")
      this.setState({chartMode: "pie"})
    }



    if (this.state.chartMode == "pie") {
      out.push(this.renderPieChart())
    } else if (this.state.chartMode == "histogram") {
      out.push(this.renderHistogram())
    }

    const setPieChartMode = (() => {console.log("PIE CHART MODE"); this.setState({chartMode: "pie"})})
    const setHistogramMode = (() => {console.log("HISTOGRAM MODE"); this.setState({chartMode: "histogram"})})

    out.push(<div key={"toggles"}>
              <div className='btn-group btn-group-toggle' data-toggle='buttons'>
                <label className='btn btn-secondary active' onClick={setPieChartMode}>
                  <input type='radio' name='options' id='option1' autoComplete='off' onClick={setPieChartMode} defaultChecked /><img src="/images/piechart.png" height="16px" width="16px"></img> Pie
                </label>
                <label className='btn btn-secondary' onClick={setHistogramMode}>
                  <input type='radio' name='options' id='option1' autoComplete='off' onClick={setHistogramMode} /><img src="/images/histogram.png" height="16px" width="16px"></img> Histogram
                </label>
              </div>
              <div className='clear'>&nbsp;</div>
            </div>)

  return (<div>{out}</div>)
  }

  renderPieChart() {
    const colorSeq = ['#2FB0E8', '#FFC32A', '#27EE77', '#FF532A']
    const pies = []
    _(this.props.maxAttempt).times((i) => {
      const color = colorSeq[i] || this.getRandomColor()
      pies.push(<Pie data={this.props.distribution} dataKey={'value'} nameKey={'name'} />)
    })

    // console.log(this.props.distribution)

    // from http://recharts.org/en-US/examples/PieChartWithCustomizedLabel
    const RADIAN = Math.PI / 180;  
    const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
      const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
     const x  = cx + radius * Math.cos(-midAngle * RADIAN);
     const y = cy  + radius * Math.sin(-midAngle * RADIAN);


     // format "Answer Text (A)": this.props.distribution[index]["text"] + " (" + this.props.distribution[index]["answer"] + ")"
    
     return (
       <text x={x} y={y} fill="white" textAnchor={'middle'} dominantBaseline="central">
         {percent > 0 ? (this.props.distribution[index]["correct"] == true ? "✓" : "") + " " + this.props.distribution[index]["answer"] : ''} {percent > 0 ? `${(percent * 100).toFixed(0)}%` : ''}
       </text>
     );}




    return (<div>
      <PieChart
      height={300} width={300} margin={{top: 20, right: 20, left: 20, bottom: 20}}>
        <Pie data={this.generatePieChartArray()} dataKey={'value'} nameKey={"name"} fill={'#2FB0E8'} label={renderCustomizedLabel} labelLine={false}/>
      </PieChart>
    </div>)
  }

  renderHistogram () {
    const colorSeq = ['#2FB0E8', '#FFC32A', '#27EE77', '#FF532A']
    const bars = []
    _(this.props.maxAttempt).times((i) => {
      const color = colorSeq[i] || this.getRandomColor()
      bars.push(<Bar key={i} dataKey={'pct_attempt_' + (i + 1)} maxBarSize={45} fill={color} label isAnimationActive={false} />)
    })
    return (<div className='ql-center-answer-distribution'>
      <BarChart className='ql-answer-distribution'
        height={190} width={500} data={this.props.distribution}
        margin={{top: 20, right: 10, left: -25, bottom: 5}}>
        <XAxis dataKey='answer' />
        <YAxis allowDecimals={false} />
        <Legend />
        {bars}
      </BarChart>
      <div className='clear'>&nbsp;</div>
    </div>)
  } //  end render

}

export const AnswerDistribution = createContainer((props) => {
  const responseStats = props.responseStats
  const maxEntry = _(responseStats).max((s) => { return s.attempt })
  const maxAttempt = !(_.isEmpty(maxEntry)) ? maxEntry.attempt : 0
  // create the data for plotting, an array like:
  // [{answer:A, attempt_1:5, attempt_2:0}, {answer:B, attempt_1:8, attempt_2:3},... ]
  // (one object per answer)
  const answersByAttempt = _(responseStats).groupBy('answer')
  let distribution = []
  _(answersByAttempt).keys().forEach((answer) => {
    const responseStatsByAttempt = _(answersByAttempt[answer]).groupBy('attempt')
    let answerEntry = {}
    answerEntry[answer] = answer
    answerEntry["answer"] = answer
    answerEntry["text"] = props.question["options"].find(a => a["answer"] == answer)["plainText"]
    answerEntry["correct"] = props.question["options"].find(a => a["answer"] == answer)["correct"]


    _(responseStatsByAttempt).keys().forEach((aNumber) => {
      answerEntry['attempt_' + aNumber] = responseStatsByAttempt[aNumber][0]['counts']
      answerEntry['pct_attempt_' + aNumber] = responseStatsByAttempt[aNumber][0]['pct']
    })
    distribution.push(answerEntry)
  })

  return {
    distribution: distribution,
    maxAttempt: maxAttempt
  }
}, _AnswerDistribution)

AnswerDistribution.propTypes = {
  question: PropTypes.object.isRequired,
  responseStats: PropTypes.array.isRequired// distribution of answers for displaying stats
}
