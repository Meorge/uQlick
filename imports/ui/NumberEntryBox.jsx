import React, { Component } from 'react'

export const BASES = {
    bin: 2,
    hex: 16,
    oct: 8,
    dec: 10
}

export const BASE_PREFIXES = {
    bin: "0b",
    hex: "0x",
    oct: "0o",
    dec: ""
}

export class NumberEntryBox extends Component {

    constructor (props) {
        super(props)
        this.state = { "value": "", "base": props.base != null ? props.base : "dec", "numberValue": 0}
        this.props = props

        this.setValue = this.setValue.bind(this)
        this.updateBase = this.updateBase.bind(this)

        this.prefixes = {
            bin: "0b",
            hex: "0x",
            oct: "0o",
            dec: "Dec"
        }

        this.bases = {
            bin: 2,
            hex: 16,
            oct: 8,
            dec: 10
        }

        this.regexes = {
            bin: /[^01.]/ig,
            hex: /[^\da-fA-F.]/ig,
            oct: /[^0-7.]/ig,
            dec: /[^\d.]/ig
        }

    }

    // componentDidUpdate() {
    //     console.log("update")
    // }

    componentDidUpdate() {
        if (this.props.base != null) {
            if (this.props.base != this.state.base) this.updateBase(this.props.base)
        }

        let currentBase = this.bases[this.state.base]

        if (!isNaN(this.props.value) && this.props.value != this.state.numberValue) {
            console.log(this.props.value, "!=", this.state.numberValue, " so setting it to be the same")
            console.log("Base is ", currentBase, ", set text to", this.props.value.toString(currentBase))
            this.setState({
                value: this.props.value.toString(currentBase).toUpperCase(),
                numberValue: this.props.value
            })
        }
    }

    // componentDidUpdate() {
    //     console.log("Update")
    //     let currentBase = this.bases[this.state.base]
    //     if (this.props.base != null) {
    //         if (this.props.base != this.state.base) this.updateBase(this.props.base)
    //     }
    //     if (this.props.value != null && !isNaN(this.props.value) && (this.props.value != this.state.numberValue || this.props.value.toString(currentBase) != this.state.value)) {
    //         //let currentBase = this.bases[this.props.base != null ? this.props.base : this.state.base]
    //         var val = this.props.value
    //         console.log("the second if", val, typeof(val))
    //         if (isNaN(val)) {
    //             val = 0
    //             console.log("its empty so make val 0")
    //             this.setState({value: "0", numberValue: 0})
    //         } else {
    //             console.log(val, "is a number")
    //             this.setState({value: val.toString(this.bases[currentBase]), numberValue: val})
    //         }
    //     }
    // }



    setValue(e) {
        let val = e.target.value
        val = val.toUpperCase().replace(this.regexes[this.state.base], '')

        let currentBase = this.bases[this.state.base]

        var numberVal = 0
        if (currentBase == 10) {
            numberVal = parseFloat(val)
        } else {
            numberVal = parseInt(val, currentBase)
        }
        if (isNaN(numberVal)) numberVal = 0;
        console.log("numberval:", numberVal)

        this.setState({
          value: val.toUpperCase(),
          numberValue: numberVal
        })

        if (this.props.onChangeValue) this.props.onChangeValue(val, numberVal, this.state.base)
    }


    updateBase(newBase) {
        if (this.props.onChangeBase) this.props.onChangeBase(newBase)
        this.setState({base: newBase, value: this.state.numberValue.toString(this.bases[newBase]).toUpperCase()})
        
    }
  
    render() {
        var baseToUse = this.state.base

        return (
            <div className='input-group'>
                <div className="input-group-prepend">
                    {this.props.changeBase ?
                    (<div>
                        <button className="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{this.prefixes[baseToUse]}</button>
                        <div className="dropdown-menu">
                            <a className="dropdown-item" href="#" onClick={() => {this.updateBase("dec")}}>Decimal (Base 10)</a>
                            <div role="separator" className="dropdown-divider"></div>
                            <a className="dropdown-item" href="#" onClick={() => {this.updateBase("hex")}}>Hexadecimal (Base 16)</a>
                            <a className="dropdown-item" href="#" onClick={() => {this.updateBase("bin")}}>Binary (Base 2)</a>
                            <a className="dropdown-item" href="#" onClick={() => {this.updateBase("oct")}}>Octal (Base 8)</a>
                        </div>
                    </div>) : 
                    (
                        <span className='input-group-text'>{this.prefixes[baseToUse]}</span>
                    )}
                </div>
                <input type="text" className='form-control' onChange={this.setValue} value={this.state.value} />
            </div>
        )
    }
  }