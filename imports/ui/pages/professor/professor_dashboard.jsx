// QLICKER
// Author: Enoch T <me@enocht.am>
//
// professor_dashboard.jsx: professor overview page

import React, { Component } from 'react'
// import ReactDOM from 'react-dom'
import { createContainer } from 'meteor/react-meteor-data'
import _ from 'underscore'

import { StudentCourseComponent } from '../../StudentCourseComponent'
import { CreateCourseModal } from '../../modals/CreateCourseModal'
import { CreateInstitutionModal } from '../../modals/CreateInstitutionModal'

import { Courses } from '../../../api/courses'
import { Sessions } from '../../../api/sessions'
import { Institutions } from '../../../api/institutions'
import { InstitutionListItem } from '../../InstitutionListItem'

class _ProfessorDashboard extends Component {

  constructor (props) {
    super(props)

    this.state = { creatingCourse: false, edits: {} }

    this.doneCreatingCourse = this.doneCreatingCourse.bind(this)
    this.promptCreateCourse = this.promptCreateCourse.bind(this)

    this.doneCreatingInst = this.doneCreatingInst.bind(this)
    this.promptCreateInst = this.promptCreateInst.bind(this)
  }

  promptCreateCourse (e) {
    this.setState({ creatingCourse: true })
  }

  doneCreatingCourse (e) {
    this.setState({ creatingCourse: false })
  }

  promptCreateInst (e) {
    this.setState({ creatingInst: true })
  }

  doneCreatingInst (e) {
    this.setState({ creatingInst: false })
  }

  render () {
    // console.log(this.props.localAdminInsts._wrapped, _.size(this.props.localAdminInsts._wrapped), this.props.profInsts._wrapped, _.size(this.props.profInsts._wrapped))
    return (
      <div className='container ql-professor-page'>
        <div className='row'>
          <h2 className='col-sm-10 col-12'>Active Courses</h2>
          <div className='col-sm-2 col-12'>
            <button className='btn btn-success float-right' onClick={() => Router.go('courses')}><i className="material-icons">list</i> Manage</button>
          </div>
        </div>
        <div className='ql-courselist'>
          {_.size(this.props.courses._wrapped) > 0 ? this.props.courses.map((course) => (<StudentCourseComponent key={course._id} course={course} sessionRoute='session.edit' />)) : (<div className="text-center text-muted">No active courses</div>)}
        </div>
        <br /><br />
        <hr />
        <div className="row">
          <h2 className='col-sm-10 col-12'>My Institutions</h2>
          <div className='col-sm-2 col-12'>
            <button className='btn btn-success float-right' onClick={this.promptCreateInst}><i className="material-icons">add_circle_outline</i> New</button>
          </div>
        </div>
        <div className='ql-courselist'>
          {(_.size(this.props.localAdminInsts._wrapped) > 0 || _.size(this.props.profInsts._wrapped) > 0) ? '' : (<div className="text-center text-muted">No institutions</div>)}
          {this.props.localAdminInsts.map((inst) => inst ? (
          <InstitutionListItem key={inst._id} inst={inst} click={() => { Router.go('institution', { instId: inst._id }) }} controls={[]} showUserStatus={true} />) : '')}

          {this.props.profInsts.map((inst) => inst ? (
          <InstitutionListItem key={inst._id} inst={inst} click={() => { Router.go('institution', { instId: inst._id }) }} controls={[]} showUserStatus={true} />) : '')}
        </div>


        {/* modals */}
        { this.state.creatingInst ? <CreateInstitutionModal done={this.doneCreatingInst} /> : '' }
      </div>)
  }

}

export const ProfessorDashboard = createContainer(() => {
  const handle = Meteor.subscribe('courses') && Meteor.subscribe('institutions')//&& Meteor.subscribe('sessions')

  const courses = Courses.find({ instructors: Meteor.userId(), inactive: { $in: [null, false] } }).fetch()

  // There's a property for users called 'profile.profForInstitutions' that is a list of the institutions
  // for which they're a professor. Let's first get this array...
  const user = Meteor.users.findOne({_id: Meteor.userId()})

  const localAdminInsts = user.localAdminForInstitutions()//.concat(user.profForInstitutions())
  const profInsts = user.profForInstitutions()

  console.log("Institutions I've got: " + Object.keys(localAdminInsts))
  localAdminInsts.map((inst) => console.log("Institution ID=<" + inst._id + "> Name=<" + inst.name + ">"))
  /*const sessions = Sessions.find({
    courseId: { $in: _(courses).pluck('_id') },
    $or: [{ status: 'visible' }, { status: 'running' }]
  }).fetch()*/
  return {
    courses: courses,
    localAdminInsts: localAdminInsts,
    profInsts: profInsts,
    //sessions: sessions,
    loading: !handle.ready()
  }
}, _ProfessorDashboard)
