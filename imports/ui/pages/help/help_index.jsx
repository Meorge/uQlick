import React, { Component } from 'react'
// import { createContainer } from 'meteor/react-meteor-data'

import { GlobalBar } from '../home'
import { SOFTWARE_NAME } from '../../../configs'

export const ForUserType = {
  Student: 0,
  Professor: 1,
  InstitutionalAdmin: 2
}

export class HelpPage extends Component {

  constructor (props) {
    super(props)

    // console.log(props)
    this.state = {}
  }

  render() {
    
    const forOutputStrings = {
      0: "For students",
      1: "For professors",
      2: "For institutional administrators"
    }

    return (
      <div>
        <div className='container ql-home-page'>
          <br />
          <br />
          <small className="text-muted"><a href="/help" className="text-muted">Help</a> &#187; {forOutputStrings[this.props.content.for]}</small>
          <h1>{this.props.content.title}</h1>
          {this.props.content.items}
        </div>
      </div>
    )
  }
}

export class HelpBox extends Component {

  constructor (props) {
    super(props)

    console.log(props)
    this.state = {}
  }

  render() {
    switch (this.props.type) {
      case "warning":
        return (
          <div className="alert alert-warning" role="alert">
            <b>Warning</b> {this.props.content}
          </div>
        )
      case "danger":
        return (
          <div className="alert alert-danger" role="alert">
            <b>Danger</b> {this.props.content}
          </div>
        )

      default:
        return (
          <div className="alert alert-info" role="alert">
            <b>Note</b> {this.props.content}
          </div>
        )

    }

  }
}

// export const HelpBox = createContainer((props) => {
//   return {
//     content: props.content
//   }
// }, _HelpBox)


export const HelpIndex = () => (
  <div>
  <div className='container ql-home-page'>
    <br />
    <br />
    <br />
    <div className='text-center'>
      <h1>{SOFTWARE_NAME} Help</h1>
      <p>What do you need help with?</p>
      
    </div>
    <br />
    <br />


    <div className='container'>
      <div>
        <div className='card-deck mb-3'>
          <div className='mb-4 card shadow-sm d-flex'>
            <div className='card-body d-flex flex-column'>
              <h5 className='ql-card-title text-center'><img src="/images/ico_stud.svg" className="mb-3" width="100" height="100" /><br/>For Students</h5>
              <br />
              <ul className='list-unstyled'>
                <li><a href='/help/student/join-course'>Join a course</a></li>
                <li><a href='/help/student/participate-course-session'>Participate in a live course session</a></li>
                <li><a href='/help/student/review-course-session'>Review a course session</a></li>
              </ul>
            </div>
          </div>

          <div className='mb-4 card shadow-sm d-flex'>
            <div className='card-body d-flex flex-column'>
              <h5 className='ql-card-title text-center'><img src="/images/ico_prof.svg" className="mb-3" width="100" height="100" /><br/>For Professors</h5>
              <br />
              <ul className='list-unstyled'>
                <li><a href='/help/prof/create-course'>Create a course</a></li>
                <li><a href='/help/prof/create-course-session'>Create a course session</a></li>
                <li><a href='/help/prof/host-course-session'>Host a course session</a></li>
                <li><a href='/help/prof/review-grade-course-session'>Review and grade a course session</a></li>
              </ul>
            </div>
          </div>

          <div className='mb-4 card shadow-sm d-flex'>
            <div className='card-body d-flex flex-column'>
              <h5 className='ql-card-title text-center'><img src="/images/ico_inst.svg" className="mb-3" width="100" height="100" /><br/>For Institutional Administrators</h5>
              <br />
              <ul className='list-unstyled'>
                <li><a href='/help/inst/create-institution'>Create an institution</a></li>
                <li><a href='/help/inst/manage-faculty'>Manage faculty</a></li>
              </ul>
            </div>
          </div>

        </div>
      </div>
      <div>
        <div className="card-deck mb-3">
          <div className='mb-6 card shadow-sm d-flex'>
              <div className='card-body d-flex flex-column'>
                <h5 className='ql-card-title text-center'><img src="/images/ico_support.svg" className="mb-3" width="100" height="100" /><br/>Get Support</h5>
                <br />
                <p>Need more help with {SOFTWARE_NAME}?</p>
                <a href="https://docs.google.com/forms/d/e/1FAIpQLSdxG_I6WEYKsFTydwSkvZ1bJHlzy_Kr2DDESWtxeQPLRTvAnQ/viewform?usp=sf_link" className='btn btn-success mt-auto'>Contact Us</a>
              </div>
            </div>

            <div className='mb-6 card shadow-sm d-flex'>
              <div className='card-body d-flex flex-column'>
                <h5 className='ql-card-title text-center'><img src="/images/ico_bug.svg" className="mb-3" width="100" height="100" /><br/>Submit a Bug Report</h5>
                <br />
                <p>Experiencing unexpected behavior? Please submit a bug report so we can look into the issue.</p>
                <a href="https://docs.google.com/forms/d/e/1FAIpQLSe3c39MUV0v-AcTnihtQbeFN8LoY7BT5UGAp_nMayh0Uyuv8w/viewform?usp=sf_link" className='btn btn-success mt-auto'>Submit Bug Report</a>
              </div>
            </div>
          </div>
      </div>

    </div>
  </div>
  </div>
  )