import { HelpStudent_JoinCourse, HelpStudent_ParticipateCourseSession, HelpStudent_ReviewCourseSession } from './help_student'
import { HelpProf_CreateCourse, HelpProf_CreateCourseSession, HelpProf_HostCourseSession, HelpProf_ReviewGradeCourseSession } from './help_prof'
import { HelpInst_CreateInst, HelpInst_ManageFaculty } from './help_inst'


export const HelpPages = {
  student: {
    "join-course": HelpStudent_JoinCourse,
    "participate-course-session": HelpStudent_ParticipateCourseSession,
    "review-course-session": HelpStudent_ReviewCourseSession
  },
  prof: {
    "create-course": HelpProf_CreateCourse,
    "create-course-session": HelpProf_CreateCourseSession,
    "host-course-session": HelpProf_HostCourseSession,
    "review-grade-course-session": HelpProf_ReviewGradeCourseSession
  },
  inst: {
    "create-institution": HelpInst_CreateInst,
    "manage-faculty": HelpInst_ManageFaculty
  }
}