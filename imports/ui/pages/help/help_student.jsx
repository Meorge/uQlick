import React, { Component } from 'react'
import { HelpBox, ForUserType } from './help_index'

export const HelpStudent_JoinCourse = {
    "title": "Join a course",
    "for": 0,
    "items": [
                (<p>Ask your professor for a course enrollment code. Type the course enrollment code into the box labeled "Enrollment Code", and then click <b>Enroll in Course</b>.</p>),
                (<HelpBox type="info" content={<div>Your professor can also add you to their course manually.</div>} />)
            ]
}

export const HelpStudent_ParticipateCourseSession = {
    "title": "Participate in a live course session",
    "for": 0,
    "items": [
                (<p>Active course sessions will display a <b>Live</b> indicator next to them. Simply tap the live session to join it.</p>)
            ]
}

export const HelpStudent_ReviewCourseSession = {
    "title": "Review a course session",
    "for": 0,
    "items": [
                (<HelpBox type="warning" content={<div>The ability to review ended course sessions is determined by your course's professor. Not all course sessions will allow you to review.</div>} />),
                (<p>Select the course containing the course session you want to review, and then tap <b>Review</b> next to the interactive course session that you want to review. If <b>Review</b> does not show up next to the course session, it has not been enabled for review by the course's professor.</p>),
                (<p>Tap <b>Show correct</b> to show the correct answer to a question, as well as the solution explanation (if the professor has provided one).</p>),
                (<p>Tap <b>Show response</b> to show the answer you submitted during the live course session.</p>)
            ]
        }