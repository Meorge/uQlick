import React, { Component } from 'react'

export class SubmitBugReport extends Component {

    constructor (props) {
      super(props)
  
      console.log(props)
      this.state = {
          status: "waitingForReport",
          email: "",
          circumstance: "",
          problemDesc: ""
      }
      this.handleSubmit = this.handleSubmit.bind(this)
      this.handleEmailChange = this.handleEmailChange.bind(this)
      this.handleCircumstanceChange = this.handleCircumstanceChange.bind(this)
      this.handleProblemDescChange = this.handleProblemDescChange.bind(this)
    }

    handleSubmit(e) {
        console.log("submit!", e.target.checkValidity())
        if (e.target.checkValidity() == false) {
            e.target.classList.add('was-validated')
        } else {
            
            console.log(this.state.email, this.state.circumstance, this.state.problemDesc)

            this.setState({status: "attemptingToSubmit"})
            Meteor.call('submitbug', this.state.email, this.state.circumstance, this.state.problemDesc, (err) => {
                if (err) {
                    console.log(err)
                    this.setState({status: "errorSubmitting"})
                    this.setState({errorMsg: err.error})
                    this.setState({errorStack: err.stack})
                }
                else {
                    this.setState({status: "reportSubmitted"})
                }
            })
        }
        e.preventDefault();
    }

    handleEmailChange(e) {
        this.setState({email: e.target.value})
    }

    handleCircumstanceChange(e) {
        this.setState({circumstance: e.target.value})
    }

    handleProblemDescChange(e) {
        this.setState({problemDesc: e.target.value})
    }


    render() {
        var bugReportForm = (<div>                    
                    <p className="">We're sorry you're experiencing unexpected behavior. Please fill out this form so we can look into the issue.</p>
                    <form className="needs-validation" onSubmit={this.handleSubmit} noValidate>
                        <div className="mb-3">
                            <label htmlFor="email">Email <span className="text-muted">(Optional)</span></label>
                            <input type="email" className="form-control" id="email" placeholder="name@example.com" value={this.state.email} onChange={this.handleEmailChange}></input>
                            <div className="invalid-feedback">Please enter a valid email address.</div>
                        </div>
                
                        <div className="mb-3">
                            <label htmlFor="circumstances">What were you doing when the bug occurred?</label>
                            <textarea className="form-control" id="circumstances" placeholder="" rows="3" value={this.state.circumstance} onChange={this.handleCircumstanceChange} required></textarea>
                            <div className="invalid-feedback">Please explain what you were doing when the bug occurred.</div>
                        </div>
                
                        <div className="mb-3">
                            <label htmlFor="problem">What went wrong?</label>
                            <textarea className="form-control" id="problem" placeholder="" rows="3" value={this.state.problemDesc} onChange={this.handleProblemDescChange} required></textarea>
                            <div className="invalid-feedback">Please describe the bug.</div>
                        </div>
                        <button className="btn btn-primary btn-lg btn-block" type="submit">Submit Report</button>
                    </form>
        </div>)


        var submitDonePage = (
            <p>
                Thank you for submitting a bug report! We will look into this issue and contact you if we need more information.
            </p>
        )


        var pleasePanicPage = (
            <div>
                It looks like there was a problem submitting a bug report. Please send an email to <a href="mailto:m.anderson39@students.clark.edu?subject=uQlick%20Bug%20Report">m.anderson39@students.clark.edu</a> with the following information:
                <br />
                <br />
                <div className="text-monospace">
                    {this.state.errorMsg}
                    <br />
                    {this.state.errorStack}
                </div>
            </div>
        )

        var attemptingToSubmit = (
            <div className="text-center">
                <div className="spinner-border" role="status">
                    <span className="sr-only">Submitting report... Please wait.</span>
                </div>
            </div>
        )

        return (
            <div>
            <div className='container ql-home-page'>
                <br />
                <br />
                <small className="text-muted"><a href="/help" className="text-muted">Help</a></small>
                <h1>Submit a bug report</h1>
                {
                    this.state.status == "errorSubmitting" ? pleasePanicPage : 
                        (this.state.status == "reportSubmitted" ? submitDonePage : 
                        (this.state.status == "attemptingToSubmit" ? attemptingToSubmit : bugReportForm))
                }
            </div>
            </div>
        )
  
    }
  }