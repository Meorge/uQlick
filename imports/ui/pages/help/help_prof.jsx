import { HelpBox, ForUserType } from './help_index'
import React from 'react'
import { SOFTWARE_NAME } from '../../../configs'

console.log("bwwoooooop")

export const HelpProf_CreateCourse = {
    "title": "Create a course",
    "for": 1,
    "items": [
                (<p>You can create a new course from the page of an institution you are a member of.</p>),
                (<p>On the institution's page, click <b>Create Course</b>.</p>),
                (<p>On the resulting window, provide the following information:</p>),
                (<ul>
                    <li><b>Name</b> - A description of the course (e.g. <i>"Intro to Computer Science"</i>).</li>
                    <li><b>Department Code</b> - The short code for the department that the course is in (e.g. <i>"CSE"</i>).</li>
                    <li><b>Course Number</b> - The level of the course (e.g. <i>"101"</i>).</li>
                    <li><b>Section</b> - The number identifying your specific instance of the course (e.g. <i>"2134"</i>).</li>
                    <li><b>Semester</b> - The term and year of the course (e.g. <i>"W20"</i>).</li>
                </ul>),
                (<p>Click <b>Submit</b> and the course will be created.</p>)
            ]
}

export const HelpProf_CreateCourseSession = {
    "title": "Create a course session",
    "for": 1,
    "items": [
                (<p>From the course homepage, click <b>Create new interactive session/quiz</b>. Enter a name and description for the course session, and then click <b>Submit</b>. A draft session will be created.</p>),
                (<p>Click on the session to open it for editing.</p>),
                (<h3>Editor</h3>),
                (<p>The Editor is used for creating and editing course sessions. Details for each question staged for the course session will appear on the center of the window, and an outline will appear on the left-hand sidebar under <i>Question Order</i>. To add a new question to the course session, click <b>New Question</b> either within the <i>Question Order</i> sidebar or on the main page.</p>),
                (<h5>Text editor</h5>),
                (<p>{SOFTWARE_NAME} features a toolbar that allows for rich text editing.</p>),
                (<h6>Source</h6>),
                (<p>The Source button will allow you to edit the underlying HTML code for the currently selected text box. If you are proficient in HTML and need to use rich-text features, this may be faster for you than using the toolbar buttons.</p>),
                (<h6>Text attributes</h6>),
                (<ul>
                    <li>Bold</li>
                    <li>Italic</li>
                    <li>Underline</li>
                    <li>Strikethrough</li>
                    <li>Subscript</li>
                    <li>Superscript</li>
                </ul>),
                (<h6>Paragraph attributes</h6>),
                (<ul>
                    <li>Ordered list</li>
                    <li>Unordered list</li>
                    <li>Text alignment
                        <ul>
                            <li>Left align</li>
                            <li>Center align</li>
                            <li>Right align</li>
                        </ul>
                    </li>
                </ul>),
                (<h6>Links</h6>),
                (<p>Click the Insert Link button to create a new link. If you have text selected before clicking the button, that text will be converted into the resulting link.</p>),
                (<p>Paste the URL you want the link to direct to into the box labeled "URL".</p>),
                (<h6>Content</h6>),
                (<ul>
                    <li>Insert image</li>
                    <li>Insert <a href="https://en.wikibooks.org/wiki/LaTeX/Mathematics">LaTeX math equation</a></li>
                    <li>Insert table</li>
                </ul>),
                (<h6>Color</h6>),
                (<ul>
                    <li>Text color</li>
                    <li>Background color</li>
                </ul>),

                (<h5>Types of questions</h5>),
                (<p>There are five types of questions currently available in {SOFTWARE_NAME}: <b>Multiple Choice</b>, <b>Multiple Select</b>, <b>True/False</b>, <b>Short Answer</b>, and <b>Numerical</b>.</p>),
                (<h6>Multiple Choice</h6>),
                (<p>Students may choose <b>a single response</b> from up to six possible responses to the question. If you would like for students to be able to select multiple options simultaneously, use a Multiple Select question.</p>),
                (<h6>Multiple Select</h6>),
                (<p>Students may choose <b>as many options as they would like</b> from up to six possible responses to the question.</p>),
                (<h6>True/False</h6>),
                (<p>Students may answer either "True" or "False" to the question.</p>),
                (<h6>Short Answer</h6>),
                (<p>Students may submit text as a response to the question.</p>),
                (<HelpBox type="warning" content={<div>Short Answer questions are not automatically graded by {SOFTWARE_NAME}.</div>} />),
                (<h6>Numerical</h6>),
                (<p>Students may submit a number as a response to the question. In addition to specifying the correct answer, a tolerance threshold may be provided. If a student's response is within the tolerance range around the correct answer, they will be automatically graded as correct.</p>)
            ]
}

export const HelpProf_HostCourseSession = {
    "title": "Host a live course session",
    "for": 1,
    "items": [
                (<p>First, open the course session you would like to make live. In the Session Editor toolbar, click <b>Run Session</b>.</p>),
                (<p>When the window displays the text <i>"Waiting for a Question..."</i>, the session will become visible to all students enrolled in the session's parent course. Students can join the live course session as described in the article <a href="/help/student/participate-course-session">Participate in a live course session</a>.</p>),

                (<div><br /><br /></div>),
                (<h3>Controlling question visibility</h3>),
                (<h5>Making questions visible or invisible to students</h5>),
                (<p>Click <b>Show Question</b> in the toolbar to make the current question visible to students.</p>),
                (<HelpBox type="danger" content={<div>In the default display mode, the professor's screen will display the correct answer to a question. If you want to project your {SOFTWARE_NAME} screen to your class, <a>use a different display mode.</a></div>} />),
                (<p>Once there is at least one student response, a graph will display the distribution of student responses. This can be toggled between a histogram and pie chart using the buttons below.</p>),
                (<p>To make the question invisible to students, click <b>Hide Question</b>. This will cause the students' screens to display <i>"Waiting for a Question..."</i>.</p>),

                (<div><br /></div>),
                (<h5>Showing students the correct answer</h5>),
                (<p>Click <b>Show Correct</b> to make the correct answer and solution explanation (if one was provided) visible to students.</p>),
                (<HelpBox type="warning" content={<div>This will make the correct answer visible event to students who have not yet submitted a response.</div>} />),

                (<div><br /></div>),
                (<h5>Showing students response statistics</h5>),
                (<p>Click <b>Show Stats</b> to make the current response distribution visible to all students. When enabled, the statistics will automatically update every time a student submits an answer.</p>),

                (<div><br /></div>),
                (<h5>Preventing students from answering while keeping the question visible</h5>),
                (<p>Click <b>Disallow Responses</b> to prevent students from submitting responses, while still allowing them to view the question and possible responses.</p>),

                (<div><br /></div>),
                (<h5>Giving students another attempt at a question</h5>),
                (<p>Click <b>New Attempt</b> to reset all students' responses to the current question. The current attempt number is visible next to the <b>New Attempt</b> button.</p>),

                (<div><br /><br /></div>),
                (<h3>Screen modes</h3>),

                (<h5>Professor Mode</h5>),
                (<p>Professor Mode is the default display mode. It displays questions in the current course session on the left-hand side of the {SOFTWARE_NAME} window, and the current question's details in the main body (including the correct answer to the question).</p>),
                (<div><br /></div>),
                (<h5>Presentation Mode</h5>),
                (<p>Presentation Mode is useful if you want to make questions visible to students on a main screen or projector. It will hide sensitive information, such as the names of students who have submitted and the correct answers to questions.</p>),
                (<div><br /></div>),
                (<h5>Second Display Mode</h5>),
                (<p>Second Display Mode works much like Presentation Mode, but opens in a new window, which allows you to project it for the class while still having access to Professor Mode privately.</p>),
                (<div><br /></div>),
                (<h5>Mobile Mode</h5>),
                (<p>Mobile Mode is useful if you would like to use a smartphone to operate the course session instead of a laptop or desktop computer. It will compress most of the information and functions of Professor Mode into a smartphone-friendly interface.</p>),
                (<HelpBox type="warning" content={<div>The following features of Professor Mode are not available from Mobile Mode:
                    <ul>
                        <li>The full list of questions for the course session</li>
                        <li>The list of students currently active in the course session</li>
                        <li>Other view modes (Presentation Mode and Second Display Mode)</li>
                        <li>Links to other areas of {SOFTWARE_NAME} (Course Home, grades, et cetera) and user information</li>
                    </ul>
                </div>} />),
                (<p>To exit Mobile Mode, tap <b>Desktop View</b> in the top left corner of the screen</p>),

                (<div><br /><br /></div>),
                (<h2>Ending a course session</h2>),
                (<p>To end a course session, click <b>End Session</b> in the toolbar. This will return all currently-connected students to their {SOFTWARE_NAME} homepage, and you to the course homepage. The course session will now be labeled as <i>Ended</i>.</p>)


            ]
}

export const HelpProf_ReviewGradeCourseSession = {
    "title": "Review and grade a course session",
    "for": 1,
    "items": [
                (<h3>Viewing students' responses</h3>),
                (<p>To view students' responses on an ended course session, click on the three dots to the right of the session information, then click <b>Review results</b>. This will open a list of the session's questions.</p>),
                (<p>Clicking on a question will show the options for that question (if they exist), the correct answer, and a histogram of student responses.</p>),
                (<p>To view students' individual responses (with their names attached), click <b>Toggle list of responses</b>.</p>),
                (<div><br /><br /></div>),
                (<h3>Using {SOFTWARE_NAME}'s grading system</h3>),
                (<p>{SOFTWARE_NAME} has the ability to grade student responses for most types of questions. To automatically grade a course session, click <b>Calculate grades</b> and then confirm if necessary. Once grades are calculated, a table will show up with the results.</p>),
                (<p>To view more details on a given student's grade, click their grade percentage (with a red highlight).</p>),
                (<div><br /><br /></div>),
                (<h3>Modifying grades</h3>),
                (<p>To manually grade students' responses, click <b>Grade the session</b>. Navigate between questions using the <b>Previous question</b> and <b>Next question</b> buttons at the top of the window, and modify the grade value for each student as necessary. Make sure to click <b>Save</b> after setting a grade value.</p>),
                (<div><br /><br /></div>),
                (<h3>Opening a session for student review</h3>),
                (<p>You can also allow students to review their performance in a course session.</p>),
                (<p>To make a session open for student review, click <b>Allow Review</b> on the session item on the course's homepage.</p>),
                (<div>Ask your professor for a course enrollment code. Type the course enrollment code into the box labeled "Enrollment Code", and then click <b>Enroll in Course</b>.</div>),
                (<div><br /><br /></div>),
                (<h3>Reviewing students' overall grades</h3>),
                (<p>Click <b>Grades</b> in the header bar to view a list of all students currently enrolledi in the course and the grades they have earned in each course session.</p>),
                (<p>Note that the grades listed under each course session are in the format "[participation grade] / [actual grade]". For example, if a student answered all of the questions, but got only half of them right, their score would read "100 / 50".</p>)
            ]
}