// QLICKER
// Author: Enoch T <me@enocht.am>
//
// page_container.jsx: generic wrapper for logged in pages

import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import { PromoteAccountModal } from '../modals/PromoteAccountModal'
import { Courses } from '../../api/courses'

import { userGuideUrl, SOFTWARE_NAME } from '../../configs.js'
import $ from 'jquery'

class _PageContainer extends Component {

  constructor (props) {
    super(props)
    this.state = {
      promotingAccount: false,
      courseId: this.props && this.props.courseId ? this.props.courseId : '',
      courseCode: '',
      ssoLogoutUrl: null,
      ssoInstitution: null,
      showCourse: (this.props && this.props.courseId)
    }
    alertify.logPosition('bottom right')

    this.changeCourse = this.changeCourse.bind(this)
    this.setCourseCode = this.setCourseCode.bind(this)

    if(this.state.courseId !== '') {
      this.setCourseCode(this.state.courseId)
    }

  }

  setCourseCode (courseId) {
    Meteor.call('courses.getCourseCodePretty', courseId, (e, c) => {
      if(c) {
        this.setState({ courseCode: c})
      }
    })
  }

  componentWillMount () {
    const token =  Meteor.user() ? Meteor._localStorage.getItem('Meteor.loginToken') : undefined
    if (token){
      Meteor.call("getSSOLogoutUrl", token, (err,result) => {
        if(!err){
          this.setState({ssoLogoutUrl:result})
          Meteor.call("settings.getSSOInstitution", (err2,name) => {
            if(!err2)this.setState({ssoInstitution:name})
          })
        }
      })
    }
  }

  componentDidMount () {
    // Close the dropdown when selecting a link during mobile
    // view.
    $('.navbar-collapse .dropdown-menu').click(function () {
      $('.navbar-collapse').collapse('hide')
    })
  }

  componentWillReceiveProps (nextProps) {
    this.setState({ courseId: nextProps.courseId ? nextProps.courseId : this.state.courseId, showCourse: nextProps.courseId ? true : false })
    if(nextProps.courseId) this.setCourseCode(nextProps.courseId)
  }

  changeCourse (courseId) {
    const pageName = Router.current().route.getName()
    //TODO: double check this, that all cases are caught!
    if (!(pageName.includes('session') || pageName === 'courses' || pageName === 'professor'  || pageName === 'admin' || pageName === 'student'  || pageName === 'profile' )) Router.go(pageName, { courseId: courseId })
    else{
      Router.go('course', { courseId: courseId })
    }
  }

  render () {
    const user = Meteor.user()

    // if(!user)  Router.go('logout')
    //const isInstructor = user.isInstructorAnyCourse() // to view student submissions
    const canPromote = ((user && user.canPromote()) || false)//user.hasGreaterRole('professor') // to promote accounts
    const isAdmin = ((user && user.hasRole('admin')) || false)

    const logout = () => {
      Router.go('logout')
    }

    const togglePromotingAccount = () => { this.setState({ promotingAccount: !this.state.promotingAccount }) }

    const homePath = user ? (user.profile.roles.length > 0 ? Router.routes[user.profile.roles[0]].path() : '/select_acct') : '/'
    const coursesPage = user ? (user.hasGreaterRole('professor')
      ? Router.routes['courses'].path()
      : Router.routes['student'].path())

      : '/'

    return (
      <div className='ql-page-container'>
        <nav className='navbar navbar-expand-lg navbar-dark'>
            <div>
              <a href={homePath} className='navbar-brand'><img src="/images/logo_darkbg.svg" height="32" width="32" /> {SOFTWARE_NAME}</a>
              <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
              </button>
            </div>
            <div className='collapse navbar-collapse' id="navbarSupportedContent">
              <ul className='navbar-nav mr-auto'>
                { isAdmin
                   ? <li className="nav-item"><a className='nav-link' href={Router.routes['admin'].path()}>Admin</a></li>
                   : ''
                }
                {  this.state.showCourse
                   ? <li className="nav-item"><a className='nav-link' role='button' onClick={() => Router.go('course', { courseId: this.state.courseId })}>Course Home</a></li>
                   : ''
                }
                { /*isAdmin
                  ? <li> <a className='close-nav' href={Router.routes['results.overview'].path()}>Grades</a></li>
                  : this.state.showCourse
                    ? <li className='dropdown'><a className='close-nav' role='button' onClick={() => Router.go('course.results', { courseId: this.state.courseId })}>Grades</a></li>
                    : ''
                */}
                { this.state.showCourse
                  ? <li className='nav-item'><a className='nav-link' role='button' onClick={() => Router.go('course.results', { courseId: this.state.courseId })}>Grades</a></li>
                  : isAdmin
                    ? <li className='nav-item'><a className='nav-link' href={Router.routes['results.overview'].path()}>Grades</a></li>
                    :''
                }
                { this.state.showCourse /*&& !isAdmin*/
                  ? <li className='nav-item dropdown'>
                    <a className='nav-link' data-toggle='dropdown' role='button'
                      aria-haspopup='true' aria-expanded='false' onClick={() => Router.go('questions', { courseId: this.state.courseId })}>Question library</a>
                    </li>
                  : ''

                }

                { isAdmin
                  ? this.state.showCourse && this.state.courseId
                      ? <li className='nav-item dropdown'>
                          <a className='nav-link dropdown-toggle' href='#' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>
                             {this.state.courseCode}
                             <span className='caret' />
                           </a>
                           <ul className='dropdown-menu' >
                             <li><a className='dropdown-item' href={coursesPage} onClick={() => this.setState({ courseId: '', showCourse: false })}>All Courses</a></li>
                           </ul>
                         </li>
                      : <li className='nav-item'><a className='nav-link' href={Router.routes['courses'].path()}>Courses</a></li>
                  : user ? 
                    <li className='nav-item dropdown'>
                      <a href='#' className='nav-link dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>
                        { this.state.courseId
                          ?  this.state.courseCode
                          : 'Courses'
                        }
                      <span className='caret' />
                      </a>
                      <ul className='dropdown-menu' >
                        <li><a className='dropdown-item' href={coursesPage} onClick={() => this.setState({ courseId: '', showCourse: false })}>All Courses</a></li>
                        <li role='separator' className='divider' >&nbsp;</li>
                        <li className='dropdown-header'>My Active Courses</li>
                        {
                          this.props.courses.map((c) => {
                            return (<li key={c._id}><a className='dropdown-item uppercase' href='#' onClick={() => this.changeCourse(c._id)}>{c.fullCourseCode()}</a></li>)
                          })
                        }
                      </ul>
                      </li>
                      : ''
               }

               <li className="nav-item"><a className="nav-link" href="/help">Help</a></li>
               <li className="nav-item"><a className="nav-link" href="/about">About</a></li>

              </ul>

              <ul className='navbar-nav navbar-right'>
                { user ?
                <li className='nav-item dropdown'>
                  <a href='#' className='nav-link dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>
                    <img src={user.getThumbnailUrl()} className='nav-profile rounded-circle' /> {user.getName()} <span className='caret' />
                  </a>
                  <ul className='dropdown-menu'>
                    <li><a className='dropdown-item' href={Router.routes['profile'].path()}>User profile</a></li>
                    {canPromote
                      ? <li><a className='dropdown-item' href='#' onClick={togglePromotingAccount}>Promote an account to professor</a></li>
                      : ''
                    }
                    <li role='separator' className='divider' />

                    {this.state.ssoLogoutUrl ?
                          <li><a className='dropdown-item' href={this.state.ssoLogoutUrl} onClick={logout}  > Log out from {SOFTWARE_NAME} and {this.state.ssoInstitution ? this.state.ssoInstitution : 'SSO' }</a></li>
                          : <li><a className='dropdown-item' href={Router.routes['logout'].path()} onClick={logout} >Log out</a></li>
                    }
                  </ul>
                </li>
                : <li className='nav-item'><a href="/login" className='nav-link'>Log In</a></li>}
              </ul>
            </div>
        </nav>

        <div className='ql-child-container'>
          { this.props.children }
          { canPromote && this.state.promotingAccount
            ? <PromoteAccountModal done={togglePromotingAccount} />
            : '' }
        </div>
      </div>)
  }
}

export const PageContainer = createContainer(props => {
  const handle = Meteor.subscribe('courses')
  const courses = Courses.find({ inactive: { $in: [null, false] } }).fetch()

  return {
    courses: courses,
    //courseId: props.courseId,
    loading: !handle.ready()
  }
}, _PageContainer)
