import React, { Component } from 'react'
import { ROLES } from '../../configs'

export class SelectAccountType extends Component {

  constructor (props) {
    super(props)

    this.state = { resetPassword: false }

    this.setUserAccountTypeStudent.bind(this)
    this.setUserAccountTypeFaculty.bind(this)
  }
    
  componentWillMount () {
  }

  setUserAccountTypeStudent() {
    console.log("IM A STUDENT")
    Meteor.call('users.changeRole', Meteor.userId(), ROLES.student, (e) => {
      if (e) return alertify.error('Error: ' + e.message)
      alertify.success('Role changed')
      Router.go("student")
    })
  }

  setUserAccountTypeFaculty() {
    console.log("IM A FACULTY")
    Meteor.call('users.changeRole', Meteor.userId(), ROLES.prof, (e) => {
      if (e) return alertify.error('Error: ' + e.message)
      alertify.success('Role changed')
      Router.go("professor")
    })
  }

  render() {
    return (
      <div>
      <div className='container ql-home-page'>
        <br />
        <br />
        <br />
        <div className='text-center'>
          <h1>Select Account Type</h1>
        </div>
        <br />
        <br />
    
    
        <div className='container'>
          <div>
            <div className='card-deck mb-3'>
              <div className='mb-6 card shadow-sm d-flex'>
                <div className='card-body d-flex flex-column'>
                  <h5 className='ql-card-title text-center'><img src="/images/ico_stud.svg" className="mb-3" width="100" height="100" /><br/>Student account</h5>
                  <br />
                  <p>Student accounts can participate in course sessions that they have been added to.</p>
                  <button onClick={this.setUserAccountTypeStudent} className='btn btn-primary mt-auto'>Create student account</button>
                </div>
              </div>
    
              <div className='mb-6 card shadow-sm d-flex'>
                <div className='card-body d-flex flex-column'>
                  <h5 className='ql-card-title text-center'><img src="/images/ico_prof.svg" className="mb-3" width="100" height="100" /><br/>Faculty account</h5>
                  <br />
                  <p>Faculty accounts can manage institutions, courses, and course sessions.</p>
                  <button onClick={this.setUserAccountTypeFaculty} className='btn btn-primary mt-auto'>Create faculty account</button>
                </div>
              </div>
    
            </div>
          </div>
    
        </div>
      </div>
      </div>
      )
  }

}