// QLICKER
// Author: Enoch T <me@enocht.am>
//
// admin_dashboard.jsx: admin overview page

import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'

import { Settings } from '../../../api/settings'

import { Courses } from '../../../api/courses'
import { Institutions } from '../../../api/institutions'

import { ProfileViewModal } from '../../modals/ProfileViewModal'

import { ManageUsers } from '../../ManageUsers'
import { ManageImages } from '../../ManageImages'
import { ManageSSO } from '../../ManageSSO'
import { ManageInstitutions } from '../../ManageInstitutions'

class _AdminDashboard extends Component {

  constructor (p) {
    super(p)

    this.state = {
      tab: 'users',
    }
    this.toggleProfileViewModal = this.toggleProfileViewModal.bind(this)
  }

  toggleProfileViewModal (userToView = null) {
    this.setState({ profileViewModal: !this.state.profileViewModal, userToView: userToView })
  }

  render () {
    const setTab = (tab) => { this.setState({ tab: tab })}

    return (
      <div>
        <div className='container-fluid'>
          <div className="row">
            <nav className="col-md-2 d-none d-md-block bg-light sidebar">
              <div className="sidebar-sticky">
                <ul className="nav flex-column">
                  <li className="nav-item"><a href="#" className="nav-link" onClick={() => setTab('users')}>Users</a></li>
                  <li className="nav-item"><a href="#" className="nav-link" onClick={() => setTab('institutions')}>Institutions</a></li>
                  <li className="nav-item"><a href="#" className="nav-link" onClick={() => setTab('server')}>Server</a></li>
                  <li className="nav-item"><a href="#" className="nav-link" onClick={() => setTab('sso')}>SSO</a></li>
                </ul>
              </div>

            </nav>
            <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
              <div className='ql-admin-settings'>
                { this.state.tab === 'users'
                  ? <ManageUsers
                      settings={this.props.settings}
                      allUsers={this.props.allUsers}
                      courseNames={this.props.courseNames}
                      loading={this.props.loading}
                      toggleProfileViewModal={this.toggleProfileViewModal} />
                  : ''
                }
                { this.state.tab === 'server'
                  ? <ManageImages settings={this.props.settings} />
                  : ''
                }
                { this.state.tab === 'sso'
                ?  <ManageSSO settings={this.props.settings}/>
                : ''
                }
                { this.state.tab === 'institutions'
                ? <ManageInstitutions
                      settings={this.props.settings} />
                : ''
                }
              </div>
            </main>
          </div>
        </div>

        {/* { this.state.profileViewModal
          ? <ProfileViewModal
            user={this.state.userToView}
            done={this.toggleProfileViewModal} />
          : <span className='ql-admin-toolbar'>
                <span className='title'>Dashboard</span>
                <span className='divider'>&nbsp;</span>
                <span className='button' onClick={() => setTab('users')}>Users</span>
                <span className='divider'>&nbsp;</span>
                <span className='button' onClick={() => setTab('institutions')}>Institutions</span>
                <span className='divider'>&nbsp;</span>
                <span className='button' onClick={() => setTab('server')}>Images</span>
                <span className='divider'>&nbsp;</span>
                <span className='button' onClick={() => setTab('sso')}>Single Sign On</span>
              </span>
        } */}
      </div>)
  }
}

export const AdminDashboard = createContainer(() => {
  console.log("CREATE CONTAINER")
  const handle = Meteor.subscribe('users.all') && Meteor.subscribe('settings') &&
                 Meteor.subscribe('courses')
  const courses = Courses.find({}, {sort: {name : 1, createdAt: -1}}).fetch()
  let courseNames = {}
  courses.map((c) => {
    courseNames[c._id] = c.courseCode().toUpperCase()+'-'+c.semester.toUpperCase()
  })
  const settings = Settings.findOne()
  const allUsers = Meteor.users.find({ }, { sort: { 'profile.roles.0': 1, 'profile.lastname': 1 } }).fetch()
  return {
    settings: settings,
    allUsers: allUsers,
    courseNames: courseNames,
    loading: !handle.ready()
  }
}, _AdminDashboard)
